---
title: "Práctica 1 Aprendizaje Automático"
author: "Adrián Peláez Vegas 25612668Y"
date: "23 de marzo de 2018"
output: pdf_document
---

# 1. Ejercicio sobre la búsqueda iterativa de óptimos

## Gradiente descendente
**1. Implementar el algoritmo de gradiente descendente.**
En este apartado se presenta una implementación del algoritmo de Gradiente Descendente general que podrá ser usado para encontrar el mínimo de la función que indiquemos, como podremos ver en los siguientes apartados.  
Parámetros del algoritmo:  
  - **nu:** Tasa de aprendizaje. Por defecto 0.05.    
  - **umbral:** Mínimo al que queremos llegar. El algoritmo se ejecutará hasta que la función, evaluada con el punto generado en la última iteración, alcance un valor inferior al umbral.  
  - **inicio:** Vector que indica los valores de las variables de los cuales partimos.  
  - **derivadas:** Lista de las derivadas parciales respecto de cada variable de la función a minimizar.  
  - **función:** Función a minimizar.  
  - **max_iter:** Número máximo de iteraciones que queremos que realice nuestro algoritmo.  
  

```{r }
#Gradiente Descendente genérico
gradienteDescendiente<-function(nu=0.05, umbral=10^(-14), inicio=c(1,1), derivadas=derivadas1, funcion=funcionE, max_iter=50){
  
  w<-c()
  #Iniciar los pesos con el punto de partida
  for (i in 1:length(inicio)) {
    w[i] <- inicio[i]
  }
  
  valores<-funcion(w)
  iter<-0
  
  k = 0;
  #Bucle principal
  while (funcion(w)>umbral && k < max_iter) {
    
    wp <- w
    
    #Actualización de las varibles haciendo uso de sus derivadas parciales
    for (i in 1:length(derivadas)) {
      w[i] <- wp[i] - nu * derivadas[[i]](wp)
    }
    
    k<-k+1
    #Guardamos valores para generar gráfica
    valores <- cbind(valores,funcion(w))
    iter <- cbind(iter,k)
  }
  
  for(i in 1:length(w)){
    cat("  w[",i, "]: ",w[i]) 
  }
  cat( "  F(w): ", funcion(w), "\n")
  cat("Iteraccion: ",k,"\n")
  
  plot(iter,valores, xlab = "Iteraciones", ylab = "Valor de función", type = "l", col = "red")
}
```

**2. Considerar la función $E(u,v)=(u^{3}e^{v-2}-4v^{3}e^{-u})^2$ .Usar gradiente descendente para encontrar un mínimo de esta función, comenzando desde el punto $(u,v)=(1,1)$ y usando una tasa de aprendizaje $\eta=0.05$.**  
**a)Calcular analíticamente y mostrar la expresión del gradiente de la función $E(u,v)$.**

Para calcular el gradiente de la función $E(u,v)=(u^{3}e^{v-2}-4v^{3}e^{-u})^2$ tendremos que calcular las derivadas parciales de esta función respecto a cada una de sus variables $u,v$:  
$$\frac{\partial{E(u,v)}}{\partial{u}}= 2(u^3e^{v-2}-4e^{-u}v^3)(3u^2e^{v-2}+4e^{-u}v^3)$$
$$\frac{\partial{E(u,v)}}{\partial{v}}=2(u^3e^{v-2}-4e^{-u}v^3)(u^3e^{v-2}-12e^{-u}v^2))$$
**b)¿Cuántas iteraciones tarda el algoritmo en obtener por primera vez un valor de $E(u,v)$ inferior a ** $10^{-14}$.   
En primer lugar definiremos la función a minimizar y sus derivadas parciales correspondientes:  

```{r }
#Función a minimizar en ejercicio 1.2:
funcionE<-function(w){
  (w[1]^3*exp(w[2]-2)-4*w[2]^3*exp(-w[1]))^2
}
#Lista de derivadas parciales de funcionE:
derivadas1<-c(function(w) 2*(w[1]^3*exp(w[2]-2)-4*exp(-w[1])*w[2]^3)*(3*w[1]^2*exp(w[2]-2)+4*exp(-w[1])*w[2]^3) , 
              function(w) 2*(w[1]^3*exp(w[2]-2)-4*exp(-w[1])*w[2]^3)*(w[1]^3*exp(w[2]-2)-12*exp(-w[1])*w[2]^2))

```
  
Una vez definidas, podremos llamar a la función gradiente, pasandole la función y derivadas que acabamos de definir como parámetros(en este caso están puestos por defecto):  

```{r}
gradienteDescendiente()
```  
  
    
Como podemos observar, en la iteración 38 se consigue un valor inferior al umbral establecido $10^{-14}$. Este valor es 8.795204e-15.  
También podemos ovserbar en el gráfico generado como va descendiendo el valor de la función a medida que aumentan las iteraciones.  
**c)En qué coordenadas $(u,v)$ se alcanzó por primera vez un valor igual o menor a $10^{-14}$ en el apartado anterior.**
Estas coordenadas son : u=1.119544 y v=0.6539881

**3. Considerar ahora la función $f(x,y)=(x-2)^2+2(y+2)^2+2sin(2\pi{x})sin(2\pi{y})$**  
**a) Usar gradiente descendente para minimizar esta función. Usar como punto inicial $(1,1)$, tasa de aprendizaje $\eta{0.01}$ y un máximo de 50 iteraciones. Generar un gráfico de como desciende el valor de la función con las iteraciones. Repetir el experimento pero usando $\eta{0.1}$, comentar las diferencias y su dependencia de $\eta$.**  
En primer lugar, tendremos que definir la función a minimizar y sus derivadas parciales correspondientes:  

```{r}
#Función a minimizar en Ejercicio 1.3:
funcionF<-function(w){
  (w[1]-2)^2+2*(w[2]+2)^2+2*sin(2*pi*w[1])*sin(2*pi*w[2])
}
#Lista de derivadas parciales de funcionF:
derivadas2<-c(function(w) 4*pi*cos(2*pi*w[1])*sin(2*pi*w[2])+2*(w[1]-2) , 
              function(w) 4*pi*sin(2*pi*w[1])*cos(2*pi*w[2])+4*(w[2]+2))

```  

Una vez definidas, ejecutamos nuestro algoritmo Gradiente Descendente con la función y derivadas correspondientes, con la tasa de aprendizaje y el punto de inicio que se indica en el enunciado:
```{r}
gradienteDescendiente(nu=0.01, derivadas=derivadas2, funcion=funcionF)
```

Ahora repetiremos el experimento usando $\eta{=0.1}$ y compararemos resultados:  
```{r}
gradienteDescendiente(nu=0.1, umbral = -100,  derivadas=derivadas2, funcion=funcionF)
```

Como podemos ver facilmente comparando las gráficas, el cambio de $\eta$ ha originado que el resultado obtenido cambie drásticamente. Mientras que en la primera ejecución, con $\eta{=0.01}$, hemos obtenido un descenso de los valores de la función que converge de forma asintótica, lo cual deseamos que sea asi, en la segunda ejecución no se observa ninguna convergencia, los valores de la función van variando de forma desordenada, a veces aumentando su valor y a veces disminuyéndolo, lo cual es síntoma de que no estamos haciendo bien nuestra búsqueda del óptimo. El valor mínimo obtenido en la segunda ejecución es menor que el obtenido en la primera, pero no podemos decir que sea un valor fiable ya que este valor fluctúa mucho durante las iteraciones y es muy probable que ejecutandolo más iteraciones siga fluctuandonos, dandonos valores mayores que el obtenido con 50 iteraciones, y nunca convergiendo a un mínimo.  

**b) Obtener el valor mínimo y los valores de las variables $(x,y)$ en donde se alcanzan cuando el punto de inicio se fija: $(2.1,-2.1), (3,-3), (1.5,1.5), (1,-1)$. Generar una tabla con los valores obtenidos**  

Punto inicial $(2.1,-2.1)$:  
```{r}
#Ejecución del gradiente con punto inicial (2.1,-2.1):
gradienteDescendiente(nu=0.01,umbral = -100, inicio = c(2.1,-2.1), 
                      derivadas = derivadas2, funcion = funcionF)
```  
  
Punto inicial $(3,-3)$:  
```{r}
#Ejecución del gradiente con punto inicial (3,-3):
gradienteDescendiente(nu=0.01,umbral = -100, inicio = c(3,-3), 
                      derivadas = derivadas2, funcion = funcionF)
```  
  
Punto inicial $(1.5,1.5)$:  
```{r}
#Ejecución del gradiente con punto inicial (1.5,1.5):
gradienteDescendiente(nu=0.01,umbral = -100, inicio = c(1.5,1.5), 
                      derivadas = derivadas2, funcion = funcionF)
```  
  
Punto inicial $(1,-1)$:  
```{r}
#Ejecución del gradiente con punto inicial (1,-1):
gradienteDescendiente(nu=0.01,umbral = -100, inicio = c(1,-1), 
                      derivadas = derivadas2, funcion = funcionF)
```  
  
    
A continuación se generará y mostrará la tabla con los valores obtenidos en nuestro experimento:  
```{r, echo=FALSE}
x1<-c(1.284043, 0.5901837, 12.88156)
x2<-c(2.243805, -2.237926, -1.820079)
x3<-c(2.730936, -2.713279, -0.3812495)
x4<-c(1.777924, 1.032057, 18.04208)
x5<-c(1.269064, -1.286721, -0.3812495)
t<-matrix(cbind(x1,x2,x3,x4,x5), byrow = TRUE, ncol = 3)
rm(x1)
rm(x2)
rm(x3)
rm(x4)
rm(x5)
colnames(t)<-c("x","y","f(x,y)")
rownames(t)<-c("(1,1)","(2.1,-2.1)","(3,-3)","(1.5,1.5)","(1,-1)")
t
```  

**4. Cuál sería su conclusión sobre la verdadera dificultad de encontrar el mínimo global de una función arbitraria**.  
Al buscar un mínimo de una función haciendo uso del algoritmo de Gradiente Descendente nos encontramos con dos principales dificultades:  
**- La elección del punto de inicio**: El algoritmo de Gradiente Descendente es un algoritmo que encuentra un mínimo local, de forma que cuando encuentre un mínimo local, no tendrá la capacidad de salir de el para encontrar otro mínimo local menor al anterior, o un mínimo global. Teniendo en cuenta que no siempre vamos a saber la forma de la función y sus características, no siempre va a ser fácil elegir un buen punto de inicio que haga que el algoritmo encuentre un buen mínimo, ya que de el depende, en parte, el mínimo local al que seremos capaces de llegar.  
**- La elección de la tasa de aprendizaje**: Al igual que la anterior, esta es también una elección crítica. De ella dependerá el buen funcionamiento de nuestro algoritmo. Si elegimos una tasa de aprendizaje muy alta, nos podrá surgir el problema de que los valores generados de la función fluctuen arbitrariamente, pudiendo tomar valores mayores al actual en iteraciones posteriores. Este caso se plantea en el ejercicio 3.a), ya que la tasa 0.1 es demasiado grande para este ejemplo y hace que nuestros valores fluctúen arbitrariamente a lo largo de la ejecución. Por contra, si elegimos una tasa de aprendizaje demasiado pequeña, nos podrá surgir el problema de que nuestros valores nunca convergan, o tarden muchas iteraciones en hacerlo, ya que de esta forma la actualización de nuestras variables en cada iteración es mínima. Tendremos que elegir una tasa de aprendizaje apropiada de forma que nuestros valores generados converjan a un mínimo, y lo hagan a una velocidad aceptable.  
En conclusión, para encontrar el mínimo global de una función arbitraria deberemos de elegir el punto de inicio adecuado, que permita llegar a dicho mínimo, y una tasa de aprendizaje que nos permita converger a el y en un tiempo aceptable. Estas elecciones son críticas y no siempre van a ser fáciles.  


# 2. Ejercicio sobre Regresión Lineal  
Este ejercicio ajusta modelos de regresión a vectores de características extraidos de imágenes de digitos manuscritos. En particular se extraen dos característcas concretas: el valor medio del nivel de gris y simetría del número respecto de su eje vertical. Solo se seleccionarán para este ejercicio las imágenes de los números 1 y 5.  
**1. Estimar un modelo de regresión lineal a partir de los datos proporcionados de dichos números (Intensidad promedio, Simetria) usando tanto el algoritmo de la pseudo-inversa como Gradiente descendente estocástico (SGD). Las etiquetas serán $(1,-1)$, una para cada vector de cada uno de los números. Pintar las soluciones obtenidas junto con los datos usados en el ajuste. Valorar la bondad del resultado usando $E_{in}$ y $E_{out}$ (para $E_{out}$ calcular las predicciones usando los datos del fichero de test).**  

En primer lugar, cargaremos los datos desde los ficheros, y los pondremos en el formato adecuado para realizar nuestro experimento:  

```{r}
#directorio de trabajo en el que se encuentran nuestros ficheros de datos:
setwd("/home/adrian/UGR/3-2/AA/R/datos")  

#Función que nos calculará la simetría de una matriz que se le pasa por parámetro:
fsimetria <- function(A){
  A = abs(A-A[,ncol(A):1])
  -sum(A)
}

## Cargar los datos de entrenamiento:

digit.train <- read.table("zip.train", quote="\"", comment.char="", stringsAsFactors=FALSE)

digitos15.train = digit.train[digit.train$V1==1 | digit.train$V1==5,]
digitos = digitos15.train[,1]    # vector de etiquetas del train
ndigitos = nrow(digitos15.train)  # numero de muestras del train

# se retira la clase y se monta una matriz 3D: 599*16*16
grises = array(unlist(subset(digitos15.train,select=-V1)),c(ndigitos,16,16))
rm(digit.train) 
rm(digitos15.train)

##Calcular la media de intensidad sobre grises:
intensidad<-c()
suma<-0
for(i in 1:599){
  suma<-0
  for (j in 1:16) {
    for (k in 1:16) {
      suma<-suma+grises[i,j,k]
    }
  }
  intensidad<-rbind(intensidad,suma/(16*16))
}

##Calcular la simetria sobre grises:
simetria<-c()
for (i in 1:599) {
  simetria<-rbind(simetria, fsimetria(grises[i,,]))
}

rm(grises)

##Recodificacion de etiquetas -> cambiar 5 por -1
digitos[digitos == 5]<-(-1)

datosTr<-vector("numeric", length = length(intensidad))
datosTr<-datosTr+1
datosTr = as.matrix(cbind(datosTr,intensidad,simetria))
colnames(datosTr)<-c("V1","V2","V3")

rm(intensidad)
rm(simetria)

```  
  
Una vez llegados a este punto, ya tenemos los datos de entrenamiento cargados y en un formato adecuado.   
Los datos de entrenamiento serán la matriz datosTr, la cual esta formada por 3 columnas : 
- La primera, será una columna de unos, que nos será util a la hora de multiplicar nuestras características por nuestros pesos en la ejecución de la regresión lineal(esta primera columna multiplicará al termino independiente de nuestra función de regresión).  
- La segunda almacena los datos de intensidad.  
- La tercera almacena los datos de simetría.  
Las etiquetas correspondientes a estos datos estarán almacenadas en el vector dígitos, correspondiendo su posición en el vector con la posición de su vector de características asociadas en la matriz datosTr.  

Repetimos el proceso para cargar nuestro datos de Test:  

  
```{r}
#directorio de trabajo en el que se encuentran nuestros ficheros de datos:
setwd("/home/adrian/UGR/3-2/AA/R/datos")  

## Cargar los datos de test:
digit.test <- read.table("zip.test", quote="\"", comment.char="", stringsAsFactors=FALSE)

digitos15.test = digit.test[digit.test$V1==1 | digit.test$V1==5,]
digitosTst = digitos15.test[,1]    # vector de etiquetas del test
ndigitosTst = nrow(digitos15.test)  # numero de muestras del test

# se retira la clase y se monta una matriz 3D: 49*16*16
grisesTst = array(unlist(subset(digitos15.test,select=-V1)),c(ndigitosTst,16,16))
grisesTst<-as.numeric(grisesTst)
grisesTst<-array(grisesTst,(c(ndigitosTst,16,16)))
rm(digit.test) 
rm(digitos15.test)


##Calcular la media de intensidad:
intensidadTst<-c()
sumat<-0
for(i in 1:49){
  sumat<-0
  for (j in 1:16) {
    for (k in 1:16) {
      sumat<-sumat+grisesTst[i,j,k]
    }
  }
  intensidadTst<-rbind(intensidadTst,sumat/(16*16))
}

##Calcular la simetria:
simetriaTst<-c()
for (i in 1:49) {
  simetriaTst<-rbind(simetriaTst, fsimetria(grisesTst[i,,]))
}

rm(grisesTst)

##Recodificacion de etiquetas -> cambiar 5 por -1
digitosTst[digitosTst == 5]<-(-1)

datosTst<-vector("numeric", length = length(intensidadTst))
datosTst<-datosTst+1
datosTst = as.matrix(cbind(datosTst,intensidadTst,simetriaTst))
colnames(datosTst)<-c("V1","V2","V3")

rm(intensidadTst)
rm(simetriaTst)
```

Ya tenemos nuestros datos de Test, los cuales se encontrarán en la matriz datosTst, en el mismo formato que los datos de entrenamiento, y sus etiquetas correspondientes se encontrarán en el vector digitosTst.  

A continuación, definiremos la función de error que usaremos en nuestra regresión, y su respectiva derivada:  


```{r}

#Funcion error:
funcionReg<-function(w,x,y){
  i<-1
  suma<-0
  while(i<=length(x[,1])){
    suma = suma + (w[1] + w[2]*x[i,2] + w[3]*x[i,3]-y[i])^2
    i = i +1
  }
  suma = suma/length(x[,1])
  suma
}

#Derivada funcion error
derivadaReg<-function(w,x,y,k){
  i<-1
  suma<-0
  while(i<=length(x[,1])){
    suma = suma + (w[1] + w[2]*x[i,2] + w[3]*x[i,3]-y[i])*x[i,k]
    i = i +1
  }
  suma = (2*suma)/length(x[,1])
  suma
}
```

A continuación se muestra la implementación de nuestros algoritmos de regresión. En este caso son el Gradiente Descendente Estocástico, y la pseudo-inversa.  

**Gradiente Descendente Estocástico:**
```{r}
#Regresion lineal con Gradiente Descendente Estocastico -> tamaño minibath = 50
regresionLinealGDS<-function(nu=0.00012,n=599, iter=6000, tama=50, inicio=c(1,1,1), x=datosTr, y=digitos){
  
  w<-inicio

  k = 0;
  
  while (k <= iter) {
    
    wp <- w
    al<-sample(1:n,tama)
    minibatchx<-x[al,]
    minibatchy<-y[al]
    
    for (i in 1:length(w)) {
      w[i] <- wp[i] - nu * derivadaReg(wp,minibatchx,minibatchy,i)
    }
    
    k<-k+1
    
  }
  
  w
}

```

**Pseudo-inversa:**  

```{r}
regresionLinealPI<-function(datos=datosTr,label=digitos){
  pseudoinversa<-solve(t(datos)%*%datos)%*%t(datos)
  pseudoinversa%*%label
}
```

Ejecución GDS: Ejecutaremos nuestro algoritmo Gradiente Descendente Estocástico con los datos de train para generar un ajuste.

```{r}
# W obtenido
w<-regresionLinealGDS()
w
# Error de entrada obtenido
funcionReg(w,datosTr,digitos)
```
  Aquí podemos ver el ajuste realizado. Es importante tener en cuenta que el algoritmo GDS se basa en disminuir el tamaño de la muestra escogiendo un subconjunto de esta mas pequeño. Esta elección se realiza de forma aleatoria, de modo que en diferentes ejecuciones se crearán subconjuntos diferentes en cada iteración, obteniendo como consecuencia resultados diferentes de $w$ y $E_{in}$ en diferentes ejecuciones.  
  En este ejemplo, con unas 6000 iteraciones conseguimos un error aceptable y en tiempo aceptable.
  
  
  Funciones para la representación de los datos y los resultados obtenidos:
```{r}
recta<-function(w,x){
  -(w[1]/w[3])-(w[2]/w[3])*x
}

repre<-function(w){
  
  datosAux<-cbind(datosTr,digitos)
  
  aux1<-matrix(datosAux[datosAux[,4]==1], ncol = 4)
  aux2<-matrix(datosAux[datosAux[,4]==(-1)], ncol = 4)
  
  plot(aux1[,2],aux1[,3], col="blue", xlim = c(-0.9,0.1), ylim = c(-250,0), xlab = "Intensidad", ylab = "Simetría")
  points(aux2[,2],aux2[,3], col="green")
  puntos<-c(seq(-1,0.2,by=0.1))
  lines(puntos,recta(w,puntos), col="red")
}
```

Aquí podemos ver graficamente los resultados obtenidos:  
```{r, echo=FALSE}
repre(w)
```

Ejecución Pseudo-Inversa: En este caso, estamos ante un método directo, que nos proporciona el mejor ajuste. Con nuestro ejemplo, es viable realizar este método, ya que obtenemos el mejor ajuste en un tiempo ínfimo, pero tenemos que tener en cuenta que estamos ante un ejemplo con muestras de 2 características, lo cual hace que sea posile el cómputo de la pseudoinversa. Ante datos con un número mas grande de características, este método empezaría a ser más costoso, ya que ejecuta el cálculo de la inversa de una matriz, lo cual es computacionalmente costoso. Ante ejemplos con muchas características sería inviable la realización del ajuste mediante la pseudoinversa.  


```{r}
wp<-regresionLinealPI()
wp
funcionReg(wp,datosTr,digitos)

```

Aquí podemos ver graficamente los resultados obtenidos:  
```{r}
repre(wp)
```
Como podemos ver, tanto en el mínimo error obtenido en la regresión, como gráficamente, el método de la Pseudo-Inversa realiza un mejor ajuste.  
  
  Aquí podemos ver los errores de entrada y salida(datos de train y datos de test) de nuestro ajuste realizado mediante el algoritmo de Gradiente Descendente Estocástico.  

```{r}
#Ein : funcionReg con los W obtenidos con GDS y los datos de train
funcionReg(w,datosTr,digitos)
#Eout : funcionReg con los W obtenidos con GDS y los datos de test
funcionReg(w,datosTst,digitosTst)
```
  
  Y aquí, mediante la psudoinversa:

```{r}
#Ein : funcionReg con los W obtenidos con GDS y los datos de train
funcionReg(wp,datosTr,digitos)
#Eout : funcionReg con los W obtenidos con GDS y los datos de test
funcionReg(wp,datosTst,digitosTst)
```  
  
  Como ya comentamos anteriormente, la pseudoinversa nos da un mejor resultado. Aumentando el número de iteraciones del algoritmo de Gradiente Descendente Estocástico podriamos obtener un mejor resultado.  
  
**2. En este apartado exploramos como se transforman los errores $E_{in}$ y $E_{out}$ cuando aumentamos la complejidad del modelo lineal usado. Ahora hacemos uso de la función simula_unif, que nos devuelve N coordenadas 2D de puntos uniformemente muestreados dentro de una area cuadrada definida.**
**EXPERIMENTO:**
**a) Generar una muestra de entrenamiento $N=1000$ puntos en el cuadrado $X=(-1,1)x(-1,1)$. Pintar el mapa 2D.**  
**b) Consideremos la función $f(x,y)=sign((x-0.2)^2+y^2-0.6)$ que usaremos para asignar una etiqueta a cada punto de la muestra anterior. Introduciremos ruido a las etiquetas cambiando aleatoriamente el signo al 10 por ciento de ellas. Pintar el mapa de las etiquetas con ruido.**  
**c) Usando como vector de características $(1,x,y)$ ajustar un modelo de regresión lineal al conjunto de datos generado y estimar los pesos w. Estimar el error de entrada usando Gradiente Descendente Estocástico. **  
**d) Ejecutar todo el experimento 1000 veces, generando 1000 muestras diferentes**  
 - Calcular el valor medio de los errores de entrada de las 1000 muestras  
 - Generar 1000 puntos nuevos en cada iteración y calcular con ellos el valor del error de salida en cada iteración. Calcular el valor medio del error de salida en todas las iteraciones.    
**e) Valore que tan bueno considera que es el ajuste con este modelo lineal a la vista de los valores medios de $E_{in}$ y $E_{out}$ **  
  Creación de las muestras y representación, y aplicación del ruido:
  
```{r}
simula_unif = function (N=2,dims=2, rango = c(-1,1)){
  m = matrix(runif(N*dims, min=rango[1], max=rango[2]),
             nrow = N, ncol=dims, byrow=T)
  m
}
# Generación de 1000 muestras
N<-500
n<-c()
for(i in 1:N){
  n<-rbind(n,simula_unif())
}
#Función que asignará una etiqueta a cada muestra generada según la función dada:
funcionEtiquetas<-function(x,y){
  ifelse((x-0.2)*(x-0.2)+y*y-0.6>0, 1, -1) 
}

et<-funcionEtiquetas(n[,1],n[,2])

net<-cbind(n,et)

repre2<-function(n){
  aux1<-matrix(n[n[,3]==1], ncol = 3)
  aux2<-matrix(n[n[,3]==(-1)], ncol = 3)
  plot(aux1[,1],aux1[,2], col="green", xlim = c(-1,1), ylim = c(-1,1), xlab = "", ylab = "")
  points(aux2[,1],aux2[,2], col="blue")
}

#Representación de las muestras generadas:
repre2(net)

#Introducimos ruido a las muestras que hemos generado:
noise <- function(label, p){
  result <- label * sample(c(1, -1), size=length(label), replace=TRUE, prob=c(1 - p, p))
  result
}

etp<-noise(et,0.1)

netp<-cbind(n,etp)

#Representación de las muestras con ruido:
repre2(netp)


```

  **- Experimento 1000 iteraciones:**  
  Lo realizaremos con pocas iteraciones en el gradiente descendente estocástico para que la ejecución del experimento no se alargue demasiado. Aumentando este número de ejecuciones, obtendremos un mejor resultado.  
  
  
```{r}
#Experimento 1000 iteraciones
count<-0
sumatoria <-0
sumatoriaOut <-0

for (i in 1:1000) {
  
  n<-c()
  for(i in 1:N){
    n<-rbind(n,simula_unif())
  }
  
  et<-funcionEtiquetas(n[,1],n[,2])
  
  net<-cbind(n,et)

  etp<-noise(et,0.1)
  
  netp<-cbind(n,etp)
  
  naux<-vector("numeric", length = length(n[,1]))
  naux<-naux+1
  naux = as.matrix(cbind(naux,n[,1],n[,2]))
  colnames(naux)<-c("V1","V2","V3")
  
  wprima=regresionLinealGDS(nu = 0.08, n=1000, iter=10, tama=500, inicio = c(1,1,1), x=naux, y=etp)
  
  sumatoria = sumatoria + funcionReg(wprima, naux, etp)
  
  n<-c()
  for(i in 1:N){
    n<-rbind(n,simula_unif())
  }
  
  et<-funcionEtiquetas(n[,1],n[,2])
  
  net<-cbind(n,et)
  
  etp<-noise(et,0.1)
  
  netp<-cbind(n,etp)
  
  naux<-vector("numeric", length = length(n[,1]))
  naux<-naux+1
  naux = as.matrix(cbind(naux,n[,1],n[,2]))
  colnames(naux)<-c("V1","V2","V3")
  
  sumatoriaOut = sumatoriaOut + funcionReg(wprima, naux, etp)
}

media=sumatoria/1000
mediaOut = sumatoriaOut/1000
# Media de los errores de entrada:
media
# Media de los errores de salida:
mediaOut

```
  
  Conclusión: No se obtiene un buen ajuste, por varios motivos:  
  - El experimento se realiza con un número pequeño de iteraciones en el gradiente, por lo tanto nunca encuentra un buen mínimo. Aumentando este número de iteraciones tendriamos una mejor solución, pero sería bastante mas lento, ya que el experimento se ejecuta 1000 veces.  
  - Las muestras que tenemos están generadas de manera aleatoria sobre un area determinada, de forma que se distribuyen con cierta uniformidad por este area, lo cual conlleva que sea dificil realizar un ajuste con un modelo lineal.  
  Respecto a los $E_{in}$ y $E_{out}$ vemos que salen bastante elevados, y muy similares, pero aun así se puede observar que el error de entrada es un poco menor que el de salida, lo cual es lógico, ya que con las muestras de entrada se entrena nuestro modelo. El hecho de que las muestras se distribuyan por el area con cierta uniformidad, ayuda a que ambos errores tengan un valor cercano.  
  